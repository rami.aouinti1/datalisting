<?php

namespace App\Tests;

use Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegistrationTest
 * @package App\Tests
 */
class RegistrationTest extends WebTestCase
{
    /**
     * @param string $role
     * @dataProvider provideRoles
     */
    public function testSuccessfulRegistration(string $role): void
    {
        $client = static::createClient();

        $router = $client->getContainer()->get("router");

        $crawler = $client->request(Request::METHOD_GET, $router->generate("registration", [
            "role" => $role
        ]));

        $form = $crawler->filter("form[name=registration]")->form([
            "registration[email]" => "email@email.com",
            "registration[plainPassword]" => "password",
            "registration[firstname]" => "Rami",
            "registration[lastname]" => "Aouinti"
        ]);

        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    }

    /**
     * @return Generator
     */
    public function provideRoles(): Generator
    {
        yield ['producer', [
            "registration[email]" => "email@email.com",
            "registration[plainPassword]" => "password",
            "registration[firstName]" => "Rami",
            "registration[lastName]" => "Aouinti",
            "registration[farm][name]" => "Exploitation"
        ]];
        yield ['producer', [
            "registration[email]" => "email@email.com",
            "registration[plainPassword]" => "password",
            "registration[firstName]" => "Rami",
            "registration[lastName]" => "Aouinti",
            "registration[farm][name]" => "Ferme"
        ]];
        yield ['customer', [
            "registration[email]" => "email@email.com",
            "registration[plainPassword]" => "password",
            "registration[firstName]" => "Rami",
            "registration[lastName]" => "Aouinti"
        ]];
    }
}
